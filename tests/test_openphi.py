# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

OpenPhi, an API to access Philips iSyntax images.

Note that the SDK (v2.0) should be preliminarly installed:
https://www.openpathology.philips.com/.

This module roughly follows the OpenSlide Python API, see documentation at:
https://openslide.org/api/python/.

WARNING: The class is not thread-safe!

Kimmo Kartasalo, kimmo.kartasalo@gmail.com,
Nita Mulliqi, mulliqi.nita@gmail.com.

April, 2021.

"""

from openphi.openphi import OpenPhi
import unittest

# Test opening files.
class FileTests(unittest.TestCase):    
    def test_folder(self):
        self.assertRaises(AssertionError, lambda: OpenPhi("/app"))
    
    def test_nofile(self):
        self.assertRaises(AssertionError, lambda: OpenPhi("/app/missing.isyntax"))

    def test_noisyntax(self):
        self.assertRaises(AssertionError, lambda: OpenPhi("/app/testslide.tiff"))

# Open and close the OpenPhi object of the test image for every test case.
# SlideTests inherits these and applies them to each test case.
class _iSyntaxTest(object):
    def setUp(self):
        self.slide = OpenPhi(self.INPUTFILE)

    def tearDown(self):
        self.slide.close()

# Test methods on a real .isyntax file.
class SlideTests(_iSyntaxTest, unittest.TestCase):
    INPUTFILE = "/app/testslide.isyntax"
    
    # Test basic metadata populated when opening an image.
    def test_dimensions(self):
        self.assertEqual(self.slide.level_count, 8)
        self.assertEqual(self.slide.dimensions, (37382, 73222))
        self.assertEqual(self.slide.level_dimensions,
                        [(37382, 73222), (18689, 36609), (9342, 18302), 
                         (4669, 9149), (2333, 4573), (1165, 2285), 
                         (581, 1141), (289, 569)])
        self.assertEqual(self.slide.level_downsamples,
                         [1, 2, 4, 8, 16, 32, 64, 128])

    # Test extraction of full metadata.
    def test_properties(self):
        self.assertEqual(self.slide.properties['openslide.bounds-height'], '73222')
        self.assertEqual(self.slide.properties['openslide.bounds-width'], '37382')
        self.assertEqual(self.slide.properties['openslide.bounds-x'], '0')
        self.assertEqual(self.slide.properties['openslide.bounds-y'], '0')
        self.assertEqual(self.slide.properties['openslide.comment'], '             ')
        self.assertEqual(self.slide.properties['openslide.mpp-x'], '0.25')
        self.assertEqual(self.slide.properties['openslide.mpp-y'], '0.25')
        self.assertEqual(self.slide.properties['openslide.objective-power'], '40')
        self.assertEqual(self.slide.properties['openslide.quickhash-1'], '')
        self.assertEqual(self.slide.properties['openslide.vendor'], 'PHILIPS')
        self.assertEqual(self.slide.properties['DICOM_ACQUISITION_DATETIME'],'20210609111602.000000')
        self.assertEqual(self.slide.properties['DICOM_MANUFACTURERS_MODEL_NAME'], 'UFS Scanner')
        self.assertEqual(self.slide.properties['DICOM_DEVICE_SERIAL_NUMBER'], 'FMT0586')

    # Test label and macro images.
    def test_associated_images(self):
        self.assertEqual(self.slide.associated_images['label'].size, (783, 733))
        self.assertEqual(self.slide.associated_images['macro'].size, (1678, 799))
        
    # Test extraction of thumbnail.
    def test_thumbnail(self):
        self.assertEqual(self.slide.get_thumbnail((2000, 2000)).size, (581, 1141))
        self.assertEqual(self.slide.get_thumbnail((256, 256)).size, (256, 256))

    # Test extraction of thumbnail - bad parameters.
    def test_thumbnail_errors(self):
        # Width is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.get_thumbnail((-1,2000)))
        # Height is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.get_thumbnail((2000,-1)))
        
    # Test reading entire WSI.
    def test_read_wsi(self):
        wsi = self.slide.read_wsi(level=4, bgvalue=255, channels="RGBA")
        self.assertEqual(wsi.size, (2333, 4573))
        self.assertEqual(wsi.mode, "RGBA")
        
        wsi = self.slide.read_wsi(level=4, bgvalue=255, channels="RGB")
        self.assertEqual(wsi.size, (2333, 4573))
        self.assertEqual(wsi.mode, "RGB")
    
    # Test reading entire WSI - bad parameters.
    def test_read_wsi_errors(self):
        # Level is not integer.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_wsi(level=0.5, bgvalue=255, channels="RGB"))
        # Level is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_wsi(level=-1, bgvalue=255, channels="RGB"))
        # Channels is not RGB or RGBA.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_wsi(level=4, bgvalue=255, channels="ABC"))
        # Bgvalue is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_wsi(level=4, bgvalue=-1, channels="RGB"))
        
    # Test reading a region.
    def test_read_region(self):
        tile = self.slide.read_region((64,64), 0, (100,100))
        self.assertEqual(tile.size, (100,100))
        self.assertEqual(tile.mode, "RGBA")
        
        tile = self.slide.read_region((64,64), 4, (100,100))
        self.assertEqual(tile.size, (100,100))
        self.assertEqual(tile.mode, "RGBA")
        
        tile = self.slide.read_region((63,63), 0, (100,100))
        self.assertEqual(tile.size, (100,100))
        self.assertEqual(tile.mode, "RGBA")
        
        tile = self.slide.read_region((63,63), 4, (100,100))
        self.assertEqual(tile.size, (100,100))
        self.assertEqual(tile.mode, "RGBA")
        
    # Test reading a region - bad parameters.
    def test_read_region_errors(self):
        # Level is not integer.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), 0.5, (100,100)))
        # Level is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), -1, (100,100)))
        # Location is not integer.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64.5,64), 0, (100,100)))
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64.5), 0, (100,100)))
        # Location is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((-1,64), 0, (100,100)))
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,-1), 0, (100,100)))
        # Size is not integer.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), 0, (100.1,100)))
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), 0, (100,100.1)))
        # Size is negative.
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), 0, (-1,100)))
        self.assertRaises(AssertionError,
                          lambda: self.slide.read_region((64,64), 0, (100,-1)))
        
if __name__ == '__main__':
    unittest.main()
