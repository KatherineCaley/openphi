# Bare essentials Docker image for testing the OpenPhi API for Philips SDK.

# Directory where the Dockerfile is built has to include:
# - philips-sdk-ubuntu_18_04
# - testslide.isyntax
FROM ubuntu:18.04

# Author and Maintainer 
MAINTAINER Kimmo Kartasalo "kimmo.kartasalo@ki.se"

# Adding the user "docker" and switching to "docker" 
RUN useradd -ms /bin/bash docker
RUN su docker

# Avoid interactive prompts
ENV DEBIAN_FRONTEND noninteractive

#  Copy input files from the host to /app in the image
RUN mkdir app
WORKDIR app/
COPY . .

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

# Install python, pip and any python packages
RUN apt-get update
RUN apt-get install -y python3.6 python3-pip
RUN python3 -m pip --no-cache-dir install --upgrade pip setuptools
RUN python3 -m pip install --no-cache-dir --use-feature=2020-resolver pillow==8.0 numpy==1.19.2

# Install Philips SDK (requires python 3.6.9)
RUN cd philips-sdk-ubuntu18_04 && ./InstallPathologySDK.sh py3 -y
RUN cd ..

